<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;



class TrickResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'trick_name' => $this->trick_name,
            'description' => $this->description,
            'where_to_use' => $this->where_to_use,
            'video_url' => $this->video_url,
            'skater_id' => $this->skater_id
          ];
    }
}
