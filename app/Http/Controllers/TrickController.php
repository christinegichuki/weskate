<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Trick;

use App\Http\Resources\TrickResource;

class TrickController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tricks = Trick::all();
        $tricks->load(['skater:id,skater_name']);
        return response()->json($tricks, 200);

    }

    public function display($number)
    {
        //
        $tricks = Trick::paginate($number);
        $tricks->load(['skater:id,skater_name']);
        return response()->json($tricks, 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $trick = Trick::create([
            'trick_name' => $request->trick_name,
            'description' => $request->description,
            'where_to_use' => $request->where_to_use,
            'video_url' => $request->video_url,
            'skater_id' => $request->skater_id,
        ]);
        $trick->load(['skater:id,skater_name']);
        
        return response()->json($trick, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
