<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Favorite;


class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $favorite = Favorite::all();
        $favorite->load(['skater:id,skater_name','trick:id,trick_name']);
        return response()->json($favorite, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $favorite = Favorite::create([
            'trick_id' => $request->trick_id,
            'skater_id' => $request->skater_id
        ]);
        $favorite->load(['skater:id,skater_name','trick:id,trick_name']);

        return response()->json($favorite, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Favorite::destroy($id);
        return response()->json(null, 204);
    }
}
