<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    //

    protected $fillable = ['skater_id', 'trick_id'];
    
    public function skater()
    {
      return $this->belongsTo(Skater::class, 'skater_id');
    }
    public function trick()
    {
      return $this->belongsTo(Trick::class, 'trick_id');
    }
}
