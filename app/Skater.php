<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skater extends Model
{
    //

    protected $fillable = ['skater_name', 'level'];

    public function tricks()
    {
      return $this->hasMany(Trick::class, 'skater_id');
    }
    public function favorites()
    {
      return $this->hasMany(Favorites::class, 'skater_id');
    }
    
}
