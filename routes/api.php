<?php

use Illuminate\Http\Request;
use App\Skater;
use App\Trick;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('skaters', 'SkaterController@store');
Route::post('favorite', 'FavoriteController@store');
Route::get('favorite', 'FavoriteController@index');
Route::delete('favorite/{id}', 'FavoriteController@destroy');
Route::post('tricks', 'TrickController@store');
Route::get('tricks/list/{number}', 'TrickController@display');
Route::get('tricks', 'TrickController@index');
