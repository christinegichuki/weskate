#### What is WeSkate?
It is a simple web microservice that allows skaters to share and favorite skate tricks

#### How to Run WeSkate
After cloning this repository, `cd` into the project directory
Install composer dependencies - `composer install`
Create a copy of the .env file `cp .env.example .env`
Open XAMPP [Installation guide is here](https://vitux.com/how-to-install-xampp-on-your-ubuntu-18-04-lts-system/) and start MySQL and Apache
Open `http://127.0.0.1:8080/phpmyadmin` on your browser and create a new database
In the .env file fill in the `, DB_PORT, DB_DATABASE, DB_USERNAME, and DB_PASSWORD` options to match the credentials of the database you just created
Run database migrations using `php artisan migrate`
Run `Php artisan serve` to run the application using the PHP development server

#### Testing the Endpoints
These are the endpoints to be tested:
POST request `api/skaters`
POST request `api/favorites`
GET request `api/favorites`
DELETE request `api/favorite/{id}`
POST request `api/tricks`
GET request `api/tricks/list/{number}`
GET request `api/tricks`

Test the above endpoints using a REST client such as Insomnia or Postman